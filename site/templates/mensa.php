<?php snippet('header') ?>

<?php snippet('page-header') ?>

<h4>Hinweise zum Bestellen</h4>
<p>
    Die <b>Essensbestellung</b> ist bis zum Vortag um 09.00 Uhr möglich. Eine <b>Stornierung</b> ist am selben Tag bis 09.00 Uhr möglich.
</p>

<iframe id="blockrandom" name="iframe" src="https://kgs-rastede.l-e-o.eu" width="100%" height="700" scrolling="no" frameborder="0" title="Menüplan">
    Diese Option wird nicht korrekt funktionieren, da der aktuell eingesetzte Webbrowser keine Inline-Frames unterstützt!
</iframe>

<?php snippet('footertw') ?>