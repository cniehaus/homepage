<?php

/** @var \Kirby\Cms\Block $block */ ?>
<article class="prose lg:prose-xl prose-a:text-blue-600">

    <?= $block->text(); ?>

</article>