<div class="mb-3 code-wrapper">
    <pre class="m-0 px-2"><code class="p-0 language-<?= $block->language()->or('text') ?>"><?= $block->code() ?></code></pre>
</div>