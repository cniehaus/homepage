<!-- 
  site/blueprints/blocks/geschichtsepoche.php
 -->
<li class="timeline-item period">
  <div class="timeline-info"></div>
  <div class="timeline-marker"></div>
  <div class="timeline-content">
    <h2 class="timeline-title"><?= $block->epochenname() ?></h2>
  </div>
</li>