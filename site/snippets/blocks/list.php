<?php

/** @var \Kirby\Cms\Block $block */ ?>

<article class="prose-li:list-disc pl-16">

    <?= $block->text(); ?>

</article>