Title: Die flotten Hotten

----

Kurzname: Die flotten Hotten

----

Teamlogo:

- >
  64604422-125d-4521-ac57-b3174eec9c58.jpeg

----

Teamfarbe: bs-teamfarbe7

----

Text: [{"content":{"location":"kirby","image":["64604422-125d-4521-ac57-b3174eec9c58.jpeg"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"f97f0b5e-ae1b-48af-b244-40cd8a90373d","isHidden":false,"type":"image"},{"content":{"level":"h2","text":"Wir (Carolina, Theda, Lina, Jule und Leni) fahren mit dem Fahrrad nach Norderney, arbeiten auf einem Ponyhof und fahren dann \u00fcber Ocholt wieder zur\u00fcck nach Rastede."},"id":"07723c09-0edd-4c15-80b0-e5860028f35d","isHidden":false,"type":"heading"}]

----

Gallery:

- photo-2022-08-26-23-05-02.jpg
- tag-2.jpg
- photo-2022-08-28-12-03-22.jpg
- >
  59749404-9ff0-425d-a5b9-159cefdad33d.jpeg
- >
  82a36104-d30e-4883-baea-3708addf72f4.jpeg
- >
  282c7357-affa-4111-a3af-9feaac9f429e.jpeg
- >
  68d59c89-2b33-46d3-84bf-0ce0c50a5e98.jpeg
- >
  0e2fd61e-5a31-48ce-9246-a2937e909764.jpeg
- >
  ca43ad06-d8a6-4cfa-b3b0-aef38e015158.jpeg
- >
  e87e94a8-b427-49cd-98da-28db249ef0d9.jpeg
- >
  bc0e8a61-3ee6-437a-88da-80adfae017f0.jpeg
- >
  34188d10-f2f4-433f-855a-d4977bd1a8fc.jpeg
- >
  bff2b4f7-bb2e-4a06-87ff-9f5a99ab59df.jpeg
- >
  df2d3735-9147-498b-8b91-ce59d6a56f9c.jpeg

----

Uuid: cnIqC9qeBtBjF1qb