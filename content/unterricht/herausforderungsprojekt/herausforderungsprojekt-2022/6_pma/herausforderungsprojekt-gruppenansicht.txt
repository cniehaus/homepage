Title: PMA

----

Kurzname: PMA - Wir fahren nach Groningen

----

Teamlogo: - bild-27.08.22-um-12.39.png

----

Teamfarbe: teamfarbe1

----

Text: [{"content":{"level":"h2","text":"<p>Wir fahren nach Groningen und tauschen dabei Sachen.<\/p>"},"id":"10fe1f88-3891-4137-a430-e9e033cbff97","isHidden":false,"type":"heading"},{"content":{"location":"kirby","image":["bild-27.08.22-um-12.39.png"],"src":"","alt":"","caption":"","link":"","ratio":"","crop":"false"},"id":"35f9964b-c8ca-4a78-a979-ed2bb615107e","isHidden":false,"type":"image"}]

----

Gallery:

- pma.jpg
- img_f1fa77007fbc-1.jpeg
- img_cdebde6b0e0f-1.jpeg
- >
  14ca7e29-bdfa-4e03-a371-88950bbad26b.jpeg
- >
  801ddd4d-93d0-4d09-b357-9bd67d612583.jpeg
- >
  a6b2c150-a1da-4672-93b2-f78f016dea90.jpeg
- >
  57e2ea51-3355-477c-9362-82304b112a4c.jpeg
- >
  a1a2c960-a6f5-4b95-82b0-92e3beee5770.jpeg
- >
  5c8a9622-2f40-4d8d-989c-d3d8f8c092f5.jpeg
- >
  076e28a8-b6fd-4bb6-a233-be48cbf571a0.jpeg
- >
  a08282b2-a302-4667-88b0-0dbe65a98597.jpeg
- >
  332cadde-2fe6-411b-8a2e-52f4adc1efe4.jpeg
- >
  763462d3-78d9-45cf-8f64-dbf4cae6518d.jpeg
- >
  c41863ca-95d5-4e37-bb66-756006146fc0.jpeg

----

Uuid: J2TPXy55C283yRsd