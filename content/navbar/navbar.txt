Title: Menü. Bezeichnungen

----

Navbar:

- 
  dropdowntoggle: 'true'
  kategorietitel: Kontakt
  kategorieseite: [ ]
  unterpunkte:
    - 
      isexternal: 'false'
      linktitle: Anfahrt
      pagelink:
        - page://mg9wMgYkrIaFlCbM
      externallink: ""
      trennstrich: 'true'
      icon: geo-alt
    - 
      isexternal: 'false'
      linktitle: Schulleitung
      pagelink:
        - page://Vvqk7AgFZYXHcQbG
      externallink: ""
      trennstrich: 'false'
      icon: person-circle
    - 
      isexternal: 'false'
      linktitle: Fachbereichsleiter
      pagelink:
        - page://MdNd7iz8FCMpn2jG
      externallink: ""
      trennstrich: 'false'
      icon: person-square
    - 
      isexternal: 'false'
      linktitle: Kollegium
      pagelink: [ ]
      externallink: ""
      trennstrich: 'false'
      icon: person-lines-fill
    - 
      isexternal: 'false'
      linktitle: Sekretariate
      pagelink:
        - page://cNrTqydd2x8g4rGq
      externallink: ""
      trennstrich: 'false'
      icon: telephone-fill
    - 
      isexternal: 'false'
      linktitle: Hausmeister
      pagelink:
        - page://cNrTqydd2x8g4rGq
      externallink: ""
      trennstrich: 'true'
      icon: tools
    - 
      isexternal: 'false'
      linktitle: 'Schüler*innen Vertretung (SV)'
      pagelink:
        - page://bSp5sUOo62vU1QZC
      externallink: ""
      trennstrich: 'false'
      icon: people-fill
    - 
      isexternal: 'false'
      linktitle: Personalrat (SPR)
      pagelink:
        - page://gAuaVJzFieglbAvm
      externallink: ""
      trennstrich: 'false'
      icon: briefcase-fill
    - 
      isexternal: 'false'
      linktitle: Gleichstellungsbeauftragte
      pagelink:
        - page://tba3OyjSqcGw4uzI
      externallink: ""
      trennstrich: 'false'
      icon: person-fill
    - 
      isexternal: 'true'
      linktitle: Schulelternrat (SER)
      pagelink: [ ]
      externallink: https://www.ser-kgs.de/
      trennstrich: 'false'
      icon: people-fill
    - 
      isexternal: 'true'
      linktitle: Förderverein ⬈
      pagelink: [ ]
      externallink: https://foerderverein.kgsraste.de/
      trennstrich: 'false'
      icon: tags-fill
- 
  dropdowntoggle: 'true'
  kategorietitel: Über die Schule
  kategorieseite: [ ]
  unterpunkte:
    - 
      isexternal: 'false'
      linktitle: Leitbild
      pagelink:
        - page://Z9tm2VsMrsIBwb4r
      externallink: ""
      trennstrich: 'false'
      icon: bar-chart
    - 
      isexternal: 'false'
      linktitle: Schulprogramm
      pagelink:
        - page://zK50gumpWWjMhDE1
      externallink: ""
      trennstrich: 'false'
      icon: layout-wtf
    - 
      isexternal: 'false'
      linktitle: Schulvorstand
      pagelink:
        - page://9ghOiiXzBHr8rdLp
      externallink: ""
      trennstrich: 'false'
      icon: people-fill
    - 
      isexternal: 'false'
      linktitle: Unsere Geschichte
      pagelink:
        - page://Cg8KeI3uXYcKD5wL
      externallink: ""
      trennstrich: 'true'
      icon: book
    - 
      isexternal: 'false'
      linktitle: Übergang Grundschule / KGS
      pagelink:
        - page://FsXZSF81gmA8vf4k
      externallink: ""
      trennstrich: 'true'
      icon: box-arrow-right
    - 
      isexternal: 'false'
      linktitle: Die drei Schulzweige
      pagelink:
        - page://gPkdfOsduFxm10MG
      externallink: ""
      trennstrich: 'false'
      icon: list-ol
    - 
      isexternal: 'false'
      linktitle: Oberstufe
      pagelink:
        - page://Surdto5GGPSksT6U
      externallink: ""
      trennstrich: 'false'
      icon: star
    - 
      isexternal: 'false'
      linktitle: Abschlüsse an der KGS Rastede
      pagelink:
        - page://OD3fGm89HxlY5hW3
      externallink: ""
      trennstrich: 'true'
      icon: journal-text
    - 
      isexternal: 'false'
      linktitle: Profile und Wahlen
      pagelink:
        - page://4qlReM13q4Bf2UeM
      externallink: ""
      trennstrich: 'true'
      icon: shield-check
    - 
      isexternal: 'false'
      linktitle: Zuständigkeiten / Organigramm
      pagelink:
        - page://wLiIqRX5s15HNl8f
      externallink: ""
      trennstrich: 'true'
      icon: diagram-3
    - 
      isexternal: 'false'
      linktitle: Ausbildungsschule
      pagelink:
        - page://wqRe4vOjEEqzXOJE
      externallink: ""
      trennstrich: 'true'
      icon: award
    - 
      isexternal: 'false'
      linktitle: Unsere Schule in der Presse
      pagelink:
        - page://Ag2nsBiezh4V6D53
      externallink: ""
      trennstrich: 'false'
      icon: newspaper
- 
  dropdowntoggle: 'true'
  kategorietitel: 'Unterricht & Schulleben'
  kategorieseite: [ ]
  unterpunkte:
    - 
      isexternal: 'false'
      linktitle: Fächer
      pagelink:
        - page://r5GGlS3XnBei9xZx
      externallink: ""
      trennstrich: 'false'
      icon: easel
    - 
      isexternal: 'false'
      linktitle: Schülerfirmen
      pagelink:
        - page://Iv6N3LkRMgPr3Ado
      externallink: ""
      trennstrich: 'false'
      icon: shop
    - 
      isexternal: 'false'
      linktitle: Berufliche Orientierung
      pagelink:
        - page://WmBuvtXUaiQj7R4J
      externallink: ""
      trennstrich: 'false'
      icon: signpost-split
    - 
      isexternal: 'false'
      linktitle: Inklusion
      pagelink:
        - page://DyCJQXvtMnV9tYD5
      externallink: ""
      trennstrich: 'false'
      icon: door-open
    - 
      isexternal: 'false'
      linktitle: 'Unterstützung & Beratung'
      pagelink:
        - page://Z7zH7x8Spw00TbNj
      externallink: ""
      trennstrich: 'false'
      icon: chat-dots
    - 
      isexternal: 'false'
      linktitle: 'Schule ohne Rassismus - Schule mit Courage'
      pagelink:
        - page://XBS428kB2t2ZANOA
      externallink: ""
      trennstrich: 'false'
      icon: peace
    - 
      isexternal: 'false'
      linktitle: Erasmus
      pagelink:
        - page://dlehOwgRnV51f8Oz
      externallink: ""
      trennstrich: 'false'
      icon: globe-europe-africa
    - 
      isexternal: 'false'
      linktitle: Herausforderungsprojekt 2025
      pagelink:
        - page://jTcBf7aeTJYKdbba
      externallink: ""
      trennstrich: 'true'
      icon: lightbulb
    - 
      isexternal: 'false'
      linktitle: 'Online-Schüler*innenzeitung IRREGULäR'
      pagelink:
        - page://sRv6UzBltcawrmlT
      externallink: ""
      trennstrich: 'false'
      icon: bookmark
    - 
      isexternal: 'false'
      linktitle: Junge Kultur e.V.
      pagelink:
        - page://lhEgI9vlcOpNiuBI
      externallink: ""
      trennstrich: 'true'
      icon: music-note-beamed
    - 
      isexternal: 'false'
      linktitle: Arbeitsgemeinschaften
      pagelink:
        - page://oj2o73UyhYrHZpfN
      externallink: ""
      trennstrich: 'false'
      icon: collection
    - 
      isexternal: 'false'
      linktitle: Wettbewerbe
      pagelink:
        - page://S6uGNjfzASVhS33G
      externallink: ""
      trennstrich: 'true'
      icon: trophy
    - 
      isexternal: 'false'
      linktitle: "AG 'Für den Frieden'"
      pagelink:
        - page://88gOlr2oSHLoSTvT
      externallink: ""
      trennstrich: 'false'
      icon: peace
    - 
      isexternal: 'false'
      linktitle: Streitschlichter
      pagelink:
        - page://FIuN1VaOLkCAzCQA
      externallink: ""
      trennstrich: 'false'
      icon: chat-left-text
    - 
      isexternal: 'false'
      linktitle: Schulsanitätsdienst
      pagelink:
        - page://uQRuI6KFHzeaQxRW
      externallink: ""
      trennstrich: 'false'
      icon: people
    - 
      isexternal: 'false'
      linktitle: Schulsportassistenten
      pagelink:
        - page://Vx9yK4McvhSQeBqB
      externallink: ""
      trennstrich: 'false'
      icon: flower1
    - 
      isexternal: 'false'
      linktitle: >
        BO-Coaches. Hilfe bei der
        Berufsorientierung
      pagelink:
        - page://doUyUBJCK02dLrId
      externallink: ""
      trennstrich: 'false'
      icon: signpost
    - 
      isexternal: 'false'
      linktitle: Schulhund
      pagelink:
        - page://wa8zghDMNrIMR8GH
      externallink: ""
      trennstrich: 'false'
      icon: suit-heart
    - 
      isexternal: 'false'
      linktitle: SCHULRADELN
      pagelink:
        - page://PwKaQ3Sku8tmhzAX
      externallink: ""
      trennstrich: 'false'
      icon: bicycle
- 
  dropdowntoggle: 'true'
  kategorietitel: 'Service & Downloads'
  kategorieseite: [ ]
  unterpunkte:
    - 
      isexternal: 'false'
      linktitle: Informationen und Formulare
      pagelink:
        - page://21HnypzmhaCIrStR
      externallink: ""
      trennstrich: 'false'
      icon: paperclip
    - 
      isexternal: 'false'
      linktitle: Schulbuchlisten
      pagelink:
        - page://Tm2gYmbcfkDLMGIL
      externallink: ""
      trennstrich: 'false'
      icon: filter
    - 
      isexternal: 'false'
      linktitle: Schulbusverkehr
      pagelink:
        - page://qYcZ5CKvc0WU9YBH
      externallink: ""
      trennstrich: 'false'
      icon: truck
    - 
      isexternal: 'false'
      linktitle: Zeitraster
      pagelink:
        - page://2VPHxftm7U6chJAL
      externallink: ""
      trennstrich: 'false'
      icon: clock
    - 
      isexternal: 'false'
      linktitle: Suche
      pagelink:
        - page://ajeS0F1xR5PXwt34
      externallink: ""
      trennstrich: 'false'
      icon: search
    - 
      isexternal: 'true'
      linktitle: Eltern-Wiki
      pagelink: [ ]
      externallink: >
        https://wiki.kgs-rastede.de/de/eltern/elterninfos_kompakt
      trennstrich: 'false'
      icon: person-raised-hand
- 
  dropdowntoggle: 'false'
  kategorietitel: Kalender
  kategorieseite:
    - page://iIDmud7HvCJUSCay
  unterpunkte: [ ]

----

Uuid: chkdkigvBKqw17Qn