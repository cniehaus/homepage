Title: Herausforderungsprojekt 2024

----

Text: [{"attrs":[],"columns":[{"blocks":[{"content":{"text":"<p><em>Gemeinsam schaffen wir das! – Herausforderungsprojekt</em></p><p>Das Projekt „Herausforderung“ startet jedes Jahr im Januar. Schüler*innen der Klassenstufen 8 bekommen dabei die Gelegenheit, sich eine selbstbestimmte, außerschulische Herausforderung zu suchen und im Rahmen einer zeitlich begrenzten schulischen Auszeit von zwei Wochen eigenständig umzusetzen. Bei den Herausforderungen, die von Schüler:innen des 12. Jahrgangs sowie Studierenden begleitet werden, handelt es sich z.B. um sportliche, künstlerische oder soziale Aktivitäten, die sich an den Nachhaltigkeitszielen der UN orientieren.</p><p>Die teilnehmenden Schüler:innen haben ein halbes Jahr Zeit, ihre Herausforderung zu planen, bevor sie vor Beginn der Sommerferien für 14 Tage die Schule verlassen, frei nach dem Motto: „Raus aus der Komfortzone“ zum „Lernen mit Kopf, Herz und Hand“.</p><p>Am 29. Januar 2024 fällt der Startschuss. In einer Auftaktveranstaltung in der Sporthalle mit allen Schüler:innen des 8. Jahrgangs geht es zunächst um die Vorstellung des Projekts sowie um erste Projektideen. Ca. eine Woche später steht dann ein nächstes Treffen unter dem Motto „Mein Profil“ an, bei dem es darum geht, herauszufinden, was man gerne mag, was für Stärken man hat und was einen herausfordert. Nach einer Gruppenfindungsphase konkretisieren die Schüler:innen ihre Projektideen und präsentieren  diese schließlich dem Auswahlkomitee. Bis zum Start der Herausforderung am 5. Juni  bleibt nun noch Zeit, die Begleiter:innen kennen zu lernen  und weitere Planungsschritte zu meistern, so z.B. die vom Komitee ausgesprochenen Aufgaben, z.B. Probepacken, 70 km Radtour durchführen, Rezepte heraussuchen und kochen etc. zu erledigen.</p><p>Wir finden dieses Projekt so wichtig, weil Herausforderungen einen idealen Erfahrungsraum bieten, in dem Schüler<em>innen Fähigkeiten für eine nicht vorhersehbare Zukunft entwickeln. Herausforderungsprojekte sind lebensnah, inklusiv und integrativ. Sie vernetzen Lerninhalte zukunftsorientiert mit der Lebens- und Arbeitswelt. Das Team des Herausforderungsprojekts besteht aus Lehrer</em>innen und unserem Kooperationspartner (Initiative „Herausforderung einfach machen“), der uns bei der Umsetzung des Projekts tatkräftig unterstützt.  </p>"},"id":"6906fcc7-d74c-4d2b-9bdc-ef2ad4df21f6","isHidden":false,"type":"text"}],"id":"cce06684-b9c9-4f51-93c7-44b68c5bae46","width":"1/1"}],"id":"2822bda3-c968-41b0-a3c4-a2bbfe902270"},{"attrs":[],"columns":[{"blocks":[{"content":{"images":["file://KmMrYQRtK1J3PgV1"],"caption":"","ratio":"","crop":"false"},"id":"5a761b08-d309-4637-a2e2-abe1373fb4cb","isHidden":false,"type":"gallery"}],"id":"936e02a6-ea28-4f76-844c-94cacc513ded","width":"1/1"}],"id":"acb917ca-7ed7-42bc-9834-2a775375fc94"}]

----

Date: 2023-11-07 09:00:00

----

Datumstartseite: 2024-06-30 09:00:00

----

Togglestartseite: false

----

Author: Kirstin Westerholt

----

Tags: Herausforderungsprojekt, HFP, Projekt

----

Downloads: 

----

Uuid: QM43jMuSpYOM3XtM