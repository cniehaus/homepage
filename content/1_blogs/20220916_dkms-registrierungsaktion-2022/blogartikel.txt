Title: DKMS Registrierungsaktion 2022

----

Heading: 

----

Text: [{"content":{"text":"<p>Am Mittwoch, dem 07.09.2022 fand an der KGS Rastede eine von der Sch\u00fcler*innen Vertretung organisierte Aktion der DKMS statt.<br><br>Die DKMS hat sich selbst das Ziel gesetzt, durch das sammeln und registrieren von Stammzellenspender*innen weltweit Blutkrebspatienten eine Heilung zu erm\u00f6glichen. M\u00f6glich war eine Registrierung zun\u00e4chst nur f\u00fcr Sch\u00fcler*innen ab 17 Jahren, welche sich erst einen kleinen Vortrag in der Aula anh\u00f6rten, um sich anschlie\u00dfend registrieren zu lassen. Trotz der nicht verpflichtenden Teilnahme haben wir rund 120 Registrierungen gesammelt!<br><br>Ein gro\u00dfes Dankesch\u00f6n an alle, die sich registrieren lassen haben und wir freuen uns darauf, noch mehrere dieser Aktionen zu planen und durchzuf\u00fchren, um es noch mehr Sch\u00fcler*innen zu erm\u00f6glichen ein Leben zu retten.<br><br>PS: ihr k\u00f6nnt euch auch immer auf der DKMS Seite selbst informieren und euch ein Paket zur Registrierung nach Hause bestellen ;)<br><br>Eure SV<\/p>"},"id":"8680e9a9-35f7-4c34-8ee8-c4325fef0f25","isHidden":false,"type":"text"}]

----

Gallery:

- img_5236.jpg
- img_5292.jpg
- img_5252.jpg
- img_5269.jpg
- img_5275.jpg
- img_5271.jpg
- img_5270.jpg

----

Date: 2022-09-16 10:20:00

----

Datumstartseite: 2023-01-16 10:20:00

----

Author: Schüler*innen Vertretung

----

Tags: SV

----

Downloads: 

----

Fotoansicht: gallery

----

Uuid: KJ7YJd6lxVe1LuHJ