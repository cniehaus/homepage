Title: Erste Klappe fällt im Klassenzimmer

----

Heading: Video-AG der KGS dreht eigene Filme – Teilnahme an Wettbewerb geplant

----

Text: [{"content":{"text":"<p>Schule, Gemeinde, F\u00f6rderverein und Sponsoren investierten in die Ausr\u00fcstung. Bis zu den Sommerferien wollen die Jungs einen Kurzfilm produzieren.<\/p>"},"id":"28ec80d7-e6a0-4db8-ab36-c3a3a96a60bc","isHidden":false,"type":"text"},{"content":{"text":"<p>Angefangen hat alles mit der Kamera seiner Mutter. Die schnappte sich Lenn und drehte in seiner Freizeit gemeinsam mit seiner Schwester kurze Filme. \u201eWir haben alles M\u00f6gliche ausprobiert\u201c, erinnert sich der 14-J\u00e4hrige. \u201eFaszination Zeitlupe\u201c oder \u201eIm Leben einer Katze\u201c hei\u00dfen seine Projekte. Mittlerweile besitzt er sogar einen eigenen Camcorder. Seine Leidenschaft teilt der Achtkl\u00e4ssler mit einigen Mitsch\u00fclern. Seit Beginn des Schuljahres ist Lenn Mitglied in der Video-AG der Kooperativen Gesamtschule in Rastede. Normalerweise sind sie zu siebt. An diesem Nachmittag, dem ersten Treffen im neuen Halbjahr, br\u00fcten neben Lenn nur Kester, Vincent und Kenard \u00fcber ihren Filmideen.<\/p>"},"id":"a8fa0e86-d043-45e5-918e-ec2aad913f4e","isHidden":false,"type":"text"},{"content":{"text":"<p>AG-Leiter Lutz Weusmann hat viel vor: Er will mit seinen Sch\u00fclern an einemWettbewerb teilnehmen. Die \u201eAuricher Filmklappe\u201c ist das Ziel. Bis zu den Sommerferien hat die Gruppe Zeit. Das Thema kann sie frei w\u00e4hlen. Mit Lutz Weusmann haben die Jungs gute Chancen: Der Lehrer f\u00fcr Physik und Religion hat bereits mit anderen Sch\u00fclern an Filmwettbewerben teilgenommen und Preise abger\u00e4umt. Zun\u00e4chst hatte er im Unterricht mitseinen Sch\u00fclern Filme analysiert. \u201eAber wenn man Medienkompetenz bei den Sch\u00fclern erreichen will, ist es noch besser, wenn sie selber Filme machen\u201c, betont Weusmann.<\/p>"},"id":"82cb1683-9704-4e88-b6fd-5f0a8894c618","isHidden":false,"type":"text"},{"content":{"text":"<p>Hauptidee sei, dass seine Sch\u00fcler kritisch Fernsehen gucken, erkl\u00e4rt er. In den vergangenen Monaten bildete sich Weusmann weiter. Er nahm an der berufsbegleitenden Qualifizierung \u201eTaschengeldkino \u2013 Film und Filmen in der Schule\u201c an der Bundesakademie f\u00fcr kulturelle Bildung in Wolfenb\u00fcttel teil. Dort wurde er von Kameram\u00e4nnern, Regisseuren, Drehbuchautoren und anderen Profis in die Filmproduktion eingef\u00fchrt und darf sich nun Filmlehrer nennen. In diesem Halbjahr kann er mit der Video-AG richtig durchstarten: Die gesamte Ausr\u00fcstung ist einsatzbereit. Im vergangenen Jahr hakte es hier und da noch. So dauertees, bis das Schnittprogrammauf den neuangeschafften Laptops installiert war.<\/p><p>Dennoch drehte die Video-AG kurze Filme, unter anderem \u00fcber ihre Lieblingsorte. Vier hochwertige Kamera-Sets hat die Schule im vergangenen Halbjahr angeschafftund dazu passend vier Schnittpl\u00e4tze eingerichtet. Neben der Schule haben die Gemeinde Rastede, der F\u00f6rderverein der Schule und Sponsoren in das Projekt investiert. F\u00fcr ihren Filmbeitrag haben die Jungs an diesem Nachmittag erste Ideen entwickelt. Komisch sollte ihr Beitrag sein, finden Kester und Vincent. \u201eOder vielleicht ein Dokumentarfilm\u201c, schl\u00e4gt Lenn vor, der am liebsten alle Aufgaben \u00fcbernehmen w\u00fcrde: \u201eIch bin gerne Kameramann, Regisseur und Cutter\u201c, sagt er. Wer schauspielert, m\u00fcssen die Jungs noch kl\u00e4ren, das machen nicht alle gerne.<\/p>"},"id":"c0978ec2-bfa5-42a6-9d70-1921f5e04c84","isHidden":false,"type":"text"}]

----

Gallery: - 2016-02-06_video-ag-1.jpg

----

Date: 2016-02-11 10:20:00

----

Datumstartseite: 

----

Author: Sonja Klanke

----

Tags: Kunst, Video-AG, AGs, Wettbewerb

----

Downloads: 

----

Fotoansicht: gallery

----

Featured: false

----

Immer-sichtbar: false

----

Uuid: aP12EE8AgtkCMll5