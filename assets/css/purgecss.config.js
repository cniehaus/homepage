// Fuer weitere Optionen: https://purgecss.com/configuration.html#options

module.exports = {
  content: [
    'site/snippets/**/*.php',
    'site/templates/**/*.php',
    'site/plugins/**/*.php'
  ],

  css: [
    'assets/css/kgs.css'
  ]
}
